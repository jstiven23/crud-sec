package com.secretaria.crudsec.controller;

import com.secretaria.crudsec.dto.PersonDto;
import com.secretaria.crudsec.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    IPersonService iPersonService;

    @GetMapping("/")
    public ResponseEntity<?> findAll(){
        return iPersonService.findAll();
    }

    @PostMapping("/")
    public ResponseEntity<?> create(@RequestBody PersonDto personDto){
        return iPersonService.create(personDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateByID(@PathVariable("id") Integer id, @RequestBody PersonDto personDto){
        return iPersonService.updateById(id,personDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") Integer id){
        return iPersonService.deleteById(id);
    }

}
