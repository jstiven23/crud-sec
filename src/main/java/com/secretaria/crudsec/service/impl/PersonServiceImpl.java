package com.secretaria.crudsec.service.impl;

import com.secretaria.crudsec.dto.PersonDto;
import com.secretaria.crudsec.entity.PersonEntity;
import com.secretaria.crudsec.repository.IRepositoryPerson;
import com.secretaria.crudsec.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonServiceImpl implements IPersonService {

    @Autowired
    IRepositoryPerson iRepositoryPerson;

    /**
     * Method to find all persons in the BD
     *
     * @return ResponseEntity
     */
    @Override
    public ResponseEntity<?> findAll() {

        List<PersonEntity> personEntities = iRepositoryPerson.findAll();
        List<PersonDto> personDtos = new ArrayList<>();
        for (PersonEntity entity : personEntities) {
            personDtos.add(PersonDto.builder()
                    .id(entity.getId())
                    .nombre(entity.getName())
                    .apellido(entity.getLastName())
                    .numeroDocumento(entity.getNumDoc())
                    .tipoDocumento(entity.getTypeDoc())
                    .build());
        }

        return new ResponseEntity(personDtos, HttpStatus.OK);
    }

    /**
     * Method to insert a person into BD
     *
     * @param personDto
     * @return ResponseEntity
     */
    @Override
    public ResponseEntity<?> create(PersonDto personDto) {

        Optional<PersonEntity> personFind =
                iRepositoryPerson.findPersonEntityByNumDocAndTypeDoc(personDto.getNumeroDocumento(), personDto.getTipoDocumento());

        if (personFind.isPresent()) {
            return new ResponseEntity(personDto, HttpStatus.BAD_REQUEST);
        }
        PersonEntity personToSave = PersonEntity.builder()
                .name(personDto.getNombre())
                .lastName(personDto.getApellido())
                .numDoc(personDto.getNumeroDocumento())
                .typeDoc(personDto.getTipoDocumento())
                .build();

        iRepositoryPerson.save(personToSave);
        return new ResponseEntity(personDto, HttpStatus.CREATED);
    }

    /**
     * Method to update a person by id
     *
     * @param id
     * @param personDto
     * @return ResponseEntity
     */
    @Override
    public ResponseEntity<?> updateById(Integer id, PersonDto personDto) {

        Optional<PersonEntity> personFind = iRepositoryPerson.findById(id);
        if (personFind.isEmpty()) {
            return (ResponseEntity<?>) ResponseEntity.badRequest();
        }
        PersonEntity personToUpdate = personFind.get();
        personToUpdate.setName(personDto.getNombre());
        personToUpdate.setLastName(personDto.getApellido());
        personToUpdate.setNumDoc(personDto.getNumeroDocumento());
        personToUpdate.setTypeDoc(personDto.getTipoDocumento());

        iRepositoryPerson.save(personToUpdate);
        return new ResponseEntity(personDto, HttpStatus.OK);
    }

    /**
     * Method to delete a person by id
     *
     * @param id
     * @return ResponseEntity
     */
    @Override
    public ResponseEntity<?> deleteById(Integer id) {

        Optional<PersonEntity> personFind = iRepositoryPerson.findById(id);
        if (personFind.isEmpty()) {
            return (ResponseEntity<?>) ResponseEntity.badRequest();
        }
        iRepositoryPerson.deleteById(id);
        return new ResponseEntity(null, HttpStatus.OK);

    }
}
