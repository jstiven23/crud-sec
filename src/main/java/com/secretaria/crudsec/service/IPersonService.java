package com.secretaria.crudsec.service;

import com.secretaria.crudsec.dto.PersonDto;
import org.springframework.http.ResponseEntity;


public interface IPersonService {

    ResponseEntity<?> findAll();

    ResponseEntity<?> create(PersonDto personDto);

    ResponseEntity<?> updateById(Integer id, PersonDto personDto);

    ResponseEntity<?> deleteById(Integer id);

}
