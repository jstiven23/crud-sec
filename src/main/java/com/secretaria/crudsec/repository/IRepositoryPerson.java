package com.secretaria.crudsec.repository;

import com.secretaria.crudsec.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IRepositoryPerson extends JpaRepository<PersonEntity,Integer> {

    Optional<PersonEntity> findPersonEntityByNumDocAndTypeDoc(String numDoc,String typeDoc);


}
