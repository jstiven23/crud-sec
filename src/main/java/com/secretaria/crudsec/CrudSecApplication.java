package com.secretaria.crudsec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudSecApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrudSecApplication.class, args);
    }

}
